
const FIRST_NAME = "Ghita";
const LAST_NAME = "Cristina Theodora";
const GRUPA = "1077C";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(typeof value=='string')
        return parseInt(value);
    else if (typeof value !== 'number')
      {
         if (value !== Number(value))
            return NaN;
            if (Number.isFinite(value) === false) 
            return NaN;
      }
      if(value>Number.MAX_SAFE_INTEGER|| value< Number.MIN_SAFE_INTEGER)
      return NaN;
    
      return parseInt(value);
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

